﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace cusxp.Models
{
    public class Appreciation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AppreciationId { get; set; }

        public string Title { get; set; }

        public string Display { get; set; }

        public DateTime ExpirationDate { get; set; }

        public string Image { get; set; }
    }
}