﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cusxp.Models
{
    public class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocationId { get; set; }

        [Column]
        [Required]
        public string Name { get; set; }

        public string Image { get; set; }

        public string FlipImage { get; set; }

        public string Address { get; set; }

        public string GoogleMapsAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string EmailAddress { get; set; }

        [Column]
        [Required]
        [ForeignKey("Form")]
        public int FormId { get; set; }

        public virtual Form Form { get; set; }

        public virtual ICollection<AnswerPackage> AnswerPackages { get; set; }
    }
}