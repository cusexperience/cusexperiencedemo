﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cusxp.Models
{
    public class Form
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int FormId { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

    }
}