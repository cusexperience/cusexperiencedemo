﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cusxp.Models
{
    public class AnswerPackageVM
    {
        public string CompanyRegisteredName { get; set; }

        public string CompanyBrandName { get; set; }

        public string Country { get; set; }

        public string LocationName { get; set; }

        public int LocationId { get; set; }

        public int CompanyId { get; set; }

        public AnswerPackage AnswerPackage { get; set; }
    }
}