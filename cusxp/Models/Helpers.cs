﻿using System.ComponentModel;

namespace cusxp.Models
{
    public enum QuestionTypes
    {
        [Description("Scale")]
        Scale,

        [Description("Comment")]
        Comment,

        [Description("Select")]
        Select,

        [Description("MultiSelect")]
        MultiSelect
    }

    public class Helpers
    {
    }
}