﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cusxp.Models
{
    public class Question
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int QuestionId { get; set; }

        [Column]
        [Required]
        public int Position { get; set; }

        [Column]
        [Required]
        public string Description { get; set; }

        [Column]
        public string Type { get; set; }

        [Column]
        public string Options { get; set; }

        [NotMapped]
        public string[] SplitOptions { get
        {
            if (this.Options != null)
            {
                return this.Options.Split(new[] {','});
            }
            return null;
        } }

        [Column]
        [Required]
        [ForeignKey("Form")]
        public int FormId { get; set; }

        public virtual Form Form { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }
    }
}