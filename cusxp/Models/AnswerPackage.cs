﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace cusxp.Models
{
    public class AnswerPackage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AnswerPackageId { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        [Column]
        [Required]
        [ForeignKey("Location")]
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }

        public string Image { get; set; }

        public string ImageDescription { get; set; }

        public string Image2 { get; set; }

        public string ImageDescription2 { get; set; }

        public string Image3 { get; set; }

        public string ImageDescription3 { get; set; }

        public string GivenName { get; set; }

        public string SurName { get; set; }

        public string Email { get; set; }

        public string ContactNumber { get; set; }

        public string Country { get; set; }

        public DateTime DateCreated { get; set; }

        public bool IsAdmin { get; set; }

        public string FullName
        {
            get
            {
                var fullName = this.SurName ?? string.Empty + " " + this.GivenName ?? string.Empty;
                return string.IsNullOrWhiteSpace(fullName) ? "John Doe" : fullName;
            }
        }

        public string FullNameHTML
        {
            get
            {
                var fullName = this.SurName + " " + this.GivenName;
                return string.IsNullOrWhiteSpace(fullName) ? "John<br />Doe" : this.SurName + "<br />" + this.GivenName;
            }
        }
    }
}