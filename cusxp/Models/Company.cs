﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace cusxp.Models
{
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompanyId { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        [Column]
        [Required]
        [ForeignKey("Appreciation")]
        public int AppreciationId { get; set; }

        public virtual Appreciation Appreciation { get; set; }

        public string RegisteredName { get; set; }

        public string BrandName { get; set; }

        public string Country { get; set; }

        public string LogoImage { get; set; }

        public string Slug { get; set; }

        [Column]
        [ForeignKey("UserProfile")]
        public int? UserId { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}