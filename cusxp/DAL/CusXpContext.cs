﻿using System.Data.Entity;
using cusxp.Models;

namespace cusxp.DAL
{
    public class CusXpContext : DbContext
    {
        public DbSet<Location> Locations { get; set; }
        public DbSet<Form> Forms { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<AnswerPackage> AnswerPackages { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Appreciation> Appreciations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
        }
    }
}