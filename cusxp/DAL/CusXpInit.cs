﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Security;
using WebMatrix.WebData;
using cusxp.Models;

namespace cusxp.DAL
{
    public class CusXpInit : DropCreateDatabaseIfModelChanges<CusXpContext>
    {
        private const string administratorRole = "Administrator";
        private const string userRole = "User";

        public CusXpInit()
        {
            if (base.GetType().IsSubclassOf(new DropCreateDatabaseIfModelChanges<CusXpContext>().GetType()))
            {
                WebSecurity.InitializeDatabaseConnection("CusXpContext", "UserProfile",
                                                         "UserId", "UserName", autoCreateTables: true);
            }
        }

        protected override void Seed(CusXpContext context)
        {

            SeedMembership(context);
        }

        private void SeedMembership(CusXpContext context)
        {
            WebSecurity.InitializeDatabaseConnection("CusXpContext", "UserProfile",
                                                     "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists(administratorRole))
                Roles.CreateRole(administratorRole);
            if (!Roles.RoleExists(userRole))
                Roles.CreateRole(userRole);

            if (!WebSecurity.UserExists("administrator"))
            {
                WebSecurity.CreateUserAndAccount("administrator", "test123",
                                                 new
                                                 {
                                                     FirstName = "Prabhu",
                                                     LastName = "Sengal",
                                                     EmailAddress = "prabhu.sengal@gmail.com",
                                                     PhoneNumber = "+6597270011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }

            if (!Roles.GetRolesForUser("administrator").Contains(administratorRole))
                Roles.AddUsersToRole(new[] { "administrator" }, administratorRole);

            // 1-Nov-2013
            if (!WebSecurity.UserExists("rajna123"))
            {
                WebSecurity.CreateUserAndAccount("rajna123", "password",
                                                 new
                                                 {
                                                     FirstName = "Rajna",
                                                     LastName = "Ranjith",
                                                     EmailAddress = "rajna.ranjith@gmail.com",
                                                     PhoneNumber = "+659450011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }
            if (!Roles.GetRolesForUser("rajna123").Contains(userRole))
                Roles.AddUsersToRole(new[] { "rajna123" }, userRole);

            var userID = WebSecurity.GetUserId("administrator");

            var prabhusengal = context.UserProfiles.SingleOrDefault(a => a.UserId == userID);

            var rnd = new Random(Environment.TickCount);

            var form = new Form
            {
                Questions = new List<Question>
                        {
                            new Question
                                {
                                    Description = "How was your overall experience with us?",
                                    Type = QuestionTypes.Scale.ToString(),
                                    Position = 1,
                                    Options = null,
                                },
                            new Question
                                {
                                    Description = "Which staff(s) did you interact with?",
                                    Type = QuestionTypes.Comment.ToString(),
                                    Position = 2,
                                    Options = null
                                },
                            new Question
                                {
                                    Description = "What did you like the most?",
                                    Type = QuestionTypes.Select.ToString(),
                                    Position = 3,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                            new Question
                                {
                                    Description = "What did you dislike the most?",
                                    Type = QuestionTypes.Select.ToString(),
                                    Position = 4,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                            new Question
                                {
                                    Description = "What can we do to further improve your experience?",
                                    Type = QuestionTypes.Comment.ToString(),
                                    Position = 5,
                                    Options = null
                                },
                            new Question
                                {
                                    Description = "What do you feel you value the most?",
                                    Type = QuestionTypes.MultiSelect.ToString(),
                                    Position = 6,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                        }
            };

            var appreciation = new cusxp.Models.Appreciation
            {
                Title = "Complimentary $10 Voucher*",
                Display = "",
                ExpirationDate = new DateTime(2015, 12, 31),
                Image = @"images/AppreciationImage.png",
            };

            var company = new cusxp.Models.Company
            {
                RegisteredName = "Manoj Sharma",
                BrandName = "Manoj Sharma",
                Country = "Singapore",
                LogoImage = @"images/CompanyLogoImage.png",
                Appreciation = appreciation
            };

            company.Locations = new List<Location>
                {
                    new Location
                        {
                            Name = "General Feedback",
                            Image = @"images/LocationImage.jpg",
                            FlipImage = @"images/LocationFlipImage.png",
                            Address = @"75 Bukit Timah Road, #05-10 Boon Siew Building, Singapore 229833",
                            GoogleMapsAddress = @"http://goo.gl/maps/3BZeY",
                            PhoneNumber = "+6563388979",
                            EmailAddress = "kelvin@manojsharma.com",
                            Form =  form
                        }
                };

            context.Companies.Add(company);

            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
            }
        }
    }
}