﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Microsoft.WindowsAzure;
using Microsoft.Win32;
using cusxp.Helpers;

namespace cusxp.DAL
{
    public class StorageRepository
    {

        public CloudBlobClient _client { get; set; }
        public CloudBlobContainer _container { get; set; }

        /// <summary>
        /// Initialize Storage with Azure
        /// </summary>
        public StorageRepository()
        {
            _client = null;
            _container = null;

            // var storageAccount = CloudStorageAccount.FromConfigurationSetting("sgcarmart2");
            var storageAccount = CloudStorageAccount.Parse(
                                 ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
            _client = storageAccount.CreateCloudBlobClient();

            _container = _client.GetContainerReference("sgcarmart2");
            _container.CreateIfNotExists();

            var permissions = new BlobContainerPermissions();
            permissions.PublicAccess = BlobContainerPublicAccessType.Container;
            _container.SetPermissions(permissions);
        }


        public string SaveImageFromStream(Stream fileStream, string fileName, string contentType, int width, int height)
        {
            var resizedImage = ImageResize.SaveFromStream(fileStream, height, width);

            if (fileName == null) throw new ArgumentNullException("fileName");


            //fileName = "D:/Internship Project/CCM/SGCarMart/SGCarMart/Pictures/" + fileName + ".jpg";

            //try
            //{
            //    FileStream FileStream = new FileStream(fileName, System.IO.FileMode.Create,
            //                             System.IO.FileAccess.Write);

            //    FileStream.Write(resizedImage, 0, resizedImage.Length);

            //    FileStream.Close();
            //}
            //catch (Exception ex)
            //{
            //}
            //return fileName.ToString();

            var extension = StringExtensions.GetExtensionFromFilename(fileName);

            contentType = GetContentType(extension);

            var uniqueBlobName = fileName;

            var cblob = _container.GetBlockBlobReference(uniqueBlobName);
            //CloudBlockBlob blob = client.GetBlockBlobReference(uniqueBlobName);
            cblob.Properties.ContentType = contentType;
            cblob.UploadFromByteArray(resizedImage,0,resizedImage.Length);
            return cblob.Uri.ToString();


        }
        private string GetContentType(string extension)
        {
            string contentType = string.Empty;
            RegistryKey key = null;
            try
            {
                key = Registry.ClassesRoot.OpenSubKey(extension);
                contentType = key.GetValue("Content Type").ToString();
            }
            catch (Exception)
            {
                if (extension == ".pdf")
                    contentType = "application/pdf";
                else
                    contentType = "application/octet-stream";
            }

            return contentType;
        }

        public string SaveImageFromStream(Stream fileStream, string fileName, string contentType)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");

            var extension = StringExtensions.GetExtensionFromFilename(fileName);

            contentType = GetContentType(extension);

            var uniqueBlobName = fileName;

            var cblob = _container.GetBlockBlobReference(uniqueBlobName);
            // CloudBlockBlob blob = client.GetBlockBlobReference(uniqueBlobName);
            cblob.Properties.ContentType = contentType;
            cblob.UploadFromStream(fileStream);
            return cblob.Uri.ToString();
            //return fileName.ToString();
        }
    }
}