﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using cusxp.DAL;
using cusxp.Models;
using SendGridMail;
using SendGridMail.Transport;
using WebMatrix.WebData;


namespace cusxp.Controllers
{
    public class CompanyVM
    {
        public int CompanyId { get; set; }
        public string BrandName { get; set; }
    }

    public class HomeController : Controller
    {
        public ActionResult Index(string survey)
        {
            survey = string.IsNullOrWhiteSpace(survey) ? null : survey;
            //var db = new CusXpContext();
            //var locations = db.Locations.ToList();
            //ViewBag.Locations = locations;
            //return View(locations.First());
            //if (survey == null && User.Identity.IsAuthenticated)
            //{
            //}
            //else if (survey == null)
            //{
            //}
            if (survey != null)
            {
                var db = new CusXpContext();
                var company = db.Companies.SingleOrDefault(a => a.Slug == survey.ToLower());
                ViewBag.Survey = survey;
                if (company == null)
                    return View("Error");
                return View(company);
            }
            return View(survey);
        }

        public ActionResult Dashboard(string time, int? id)
        {
            if (!User.Identity.IsAuthenticated || !(Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("CompanyAdmin")))
            {
                return RedirectToAction("Index", new { survey = "" });
            }
            var db = new CusXpContext();
            var companies = new List<Company>();
            if (id == null)
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    return View("DashboardList", db.Companies.Select(a => new CompanyVM { CompanyId = a.CompanyId, BrandName = a.BrandName }).ToList());
                }
                else if (Roles.IsUserInRole("CompanyAdmin"))
                {
                    var userId = WebSecurity.GetUserId(User.Identity.Name);
                    var company = db.Companies.SingleOrDefault(a => a.UserId == userId);
                    if (company != null)
                        companies.Add(company);
                }
                else
                {
                    return View("Error");
                }
            }
            else
            {
                var company = db.Companies.SingleOrDefault(a => a.CompanyId == id.Value);
                companies.Add(company);

            }
            ViewBag.Companies = companies;
            ViewBag.Message = "Dashboard";
            ViewBag.CompanyId = id == null ? companies.FirstOrDefault().CompanyId : id.Value;
            ViewBag.Time = time;
            return View();
        }

        public ActionResult Search(int? id)
        {
            if (!User.Identity.IsAuthenticated || !(Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("CompanyAdmin")))
            {
                return RedirectToAction("Index", new { survey = "" });
            }
            var db = new CusXpContext();
            Company company;
            if (id == null)
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    return View("DashboardList", db.Companies.Select(a => new CompanyVM { CompanyId = a.CompanyId, BrandName = a.BrandName }).ToList());
                }
                else if (Roles.IsUserInRole("CompanyAdmin"))
                {
                    var userId = WebSecurity.GetUserId(User.Identity.Name);
                    company = db.Companies.SingleOrDefault(a => a.UserId == userId);
                }
                else
                {
                    return View("Error");
                }
            }
            else
            {
                company = db.Companies.SingleOrDefault(a => a.CompanyId == id.Value);

            }
            if (company == null)
                return View("Error");
            ViewBag.Company = company;
            return View();
        }

        public ActionResult DashboardI(int? id)
        {
            if (!User.Identity.IsAuthenticated || !(Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("CompanyAdmin")))
            {
                return RedirectToAction("Index", new { survey = "" });
            }
            var db = new CusXpContext();
            Company company;
            if (id == null)
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    return View("DashboardList", db.Companies.Select(a => new CompanyVM { CompanyId = a.CompanyId, BrandName = a.BrandName }).ToList());
                }
                else if (Roles.IsUserInRole("CompanyAdmin"))
                {
                    var userId = WebSecurity.GetUserId(User.Identity.Name);
                    company = db.Companies.SingleOrDefault(a => a.UserId == userId);
                }
                else
                {
                    return View("Error");
                }
            }
            else
            {
                company = db.Companies.SingleOrDefault(a => a.CompanyId == id.Value);

            }
            if (company == null)
                return View("Error");
            ViewBag.Company = company;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Tnc()
        {
            ViewBag.Message = "Tnc";

            return View();
        }
        
        public ActionResult PostSignup()
        {
            ViewBag.Message = "Post Sign up";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Submit(string q1, string q2a, string q3, string q3a, string q4, string q4a, string q5, string q5a, string q6, int locationId, string image, string givenName, string surName, string email, string contactNumber, string imageDescription, string image2, string imageDescription2, string image3, string imageDescription3, string country)
        {
            var db = new CusXpContext();
            var location = db.Locations.SingleOrDefault(a => a.LocationId == locationId);
            var answerPackage = new AnswerPackage
                {
                    Answers = new List<Answer>
                        {
                            new Answer
                                {
                                    Question = location.Form.Questions.SingleOrDefault(a => a.Position == 1),
                                    Score = q1
                                },
                                new Answer
                                {
                                    Question = location.Form.Questions.SingleOrDefault(a => a.Position == 2),
                                    Comments = q2a
                                },
                                new Answer
                                {
                                    Question = location.Form.Questions.SingleOrDefault(a => a.Position == 3),
                                    Score = q3,
                                    Comments = q3a
                                },
                                new Answer
                                {
                                    Question = location.Form.Questions.SingleOrDefault(a => a.Position == 4),
                                    Score = q4,
                                    Comments = q4a
                                },
                                new Answer
                                {
                                    Question = location.Form.Questions.SingleOrDefault(a => a.Position == 5),
                                    Score = q5,
                                    Comments =  q5a
                                },
                                new Answer
                                {
                                    Question = location.Form.Questions.SingleOrDefault(a => a.Position == 6),
                                    Score = q6
                                }
                        },
                    Location = location,
                    Image = string.IsNullOrWhiteSpace(image) ? null : image,
                    ImageDescription = imageDescription,
                    Image2 = string.IsNullOrWhiteSpace(image2) ? null : image2,
                    ImageDescription2 = imageDescription2,
                    Image3 = string.IsNullOrWhiteSpace(image3) ? null : image3,
                    ImageDescription3 = imageDescription3,
                    GivenName = givenName,
                    SurName = surName,
                    Email = email,
                    DateCreated = DateTime.Now.AddHours(8),
                    ContactNumber = contactNumber,
                    Country = country,
                    IsAdmin = User.Identity.IsAuthenticated && User.IsInRole("administrator") ? true : false
                };
            db.AnswerPackages.Add(answerPackage);
            db.SaveChanges();



            return Json(new { success = true, id = answerPackage.AnswerPackageId });
        }

        public ActionResult SubmitSecond(int answerPackageId, string givenName, string surName, string email, string contactNumber, string country)
        {
            var db = new CusXpContext();
            var answerPackage = db.AnswerPackages.SingleOrDefault(a => a.AnswerPackageId == answerPackageId);

            if (answerPackage == null)
            {
                return Json(new { success = false });
            }

            answerPackage.GivenName = givenName;
            answerPackage.SurName = surName;
            answerPackage.Email = email;
            answerPackage.ContactNumber = contactNumber;
            answerPackage.Country = country;

            db.SaveChanges();

            try
            {
                var emailMessage = SendGrid.GetInstance();
                emailMessage.AddTo(string.Format("{0} {1} <{2}>", givenName, surName, email));
                emailMessage.From = new MailAddress("appreciation@cusjo.com", "Appreciation CusJo");
                emailMessage.Subject = "Appreciation for sharing your experience";
                var fileContents = System.IO.File.ReadAllText(Server.MapPath(@"~/App_Data/email.txt"));
                emailMessage.Html = string.Format(fileContents, givenName, surName);
                var credentials = new NetworkCredential("khasali", "Cr0wbar1");
                var transportWeb = Web.GetInstance(credentials);
                transportWeb.Deliver(emailMessage);
            }
            catch (Exception ex)
            {
            }
            return Json(new { success = true });
        }

        public ActionResult Responses(int? locationId)
        {
            var db = new CusXpContext();
            var answerPackages = new List<AnswerPackage>();
            if (locationId != null)
                answerPackages = db.AnswerPackages.Where(a => a.LocationId == locationId).ToList();
            else
                answerPackages = db.AnswerPackages.ToList();
            return View(answerPackages);
        }

        public ContentResult CreateCompany()
        {
            var context = new CusXpContext();

            if (!WebSecurity.UserExists("geladmin"))
            {
                WebSecurity.CreateUserAndAccount("geladmin", "password",
                                                 new
                                                 {
                                                     FirstName = "Great Eastern Life",
                                                     LastName = "Admin",
                                                     EmailAddress = "gel@cusjo.com",
                                                     PhoneNumber = "+659450011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }
            if (!Roles.GetRolesForUser("geladmin").Contains("CompanyAdmin"))
                Roles.AddUsersToRole(new[] { "geladmin" }, "CompanyAdmin");

            var gelAdminID = WebSecurity.GetUserId("geladmin");

            var gelAdmin = context.UserProfiles.SingleOrDefault(a => a.UserId == gelAdminID);


            var form = new Form
            {
                Questions = new List<Question>
                        {
                            new Question
                                {
                                    Description = "How was your overall experience with us?",
                                    Type = QuestionTypes.Scale.ToString(),
                                    Position = 1,
                                    Options = null,
                                },
                            new Question
                                {
                                    Description = "Which staff(s) did you interact with?",
                                    Type = QuestionTypes.Comment.ToString(),
                                    Position = 2,
                                    Options = null
                                },
                            new Question
                                {
                                    Description = "What did you like the most?",
                                    Type = QuestionTypes.Select.ToString(),
                                    Position = 3,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                            new Question
                                {
                                    Description = "What did you dislike the most?",
                                    Type = QuestionTypes.Select.ToString(),
                                    Position = 4,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                            new Question
                                {
                                    Description = "What can we do to further improve your experience?",
                                    Type = QuestionTypes.Comment.ToString(),
                                    Position = 5,
                                    Options = null
                                },
                            new Question
                                {
                                    Description = "What do you feel you value the most?",
                                    Type = QuestionTypes.MultiSelect.ToString(),
                                    Position = 6,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                        }
            };

            var appreciation = new cusxp.Models.Appreciation
            {
                Title = "Complimentary $10 Voucher*",
                Display = "",
                ExpirationDate = new DateTime(2015, 12, 31),
                Image = @"images/AppreciationImage.png",
            };

            var company2 = new cusxp.Models.Company
            {
                RegisteredName = "Great Eastern Life",
                BrandName = "Great Eastern Life",
                Country = "Singapore",
                LogoImage = @"images/GE-CompanyLogo.png",
                Appreciation = appreciation,
                Slug = "greateasternlife",
                UserProfile = gelAdmin
            };

            company2.Locations = new List<Location>
                {
                    new Location
                        {
                            Name = "General Experience",
                            Image = @"images/GE-LocationImage-GeneralExperience.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Customer Service Center",
                            Image = @"images/GE-LocationImage-CusotomerService.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Call Center",
                            Image = @"images/GE-LocationImage-CallCenter.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Email Center",
                            Image = @"images/GE-LocationImage-EmailCentre.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        }
                };

            context.Companies.Add(company2);
            context.SaveChanges();
            return Content("Done");
        }

        [HttpPost]
        public JsonResult ImageUpload(FormCollection fc)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files.Get(0);

                HttpPostedFileBase hpImage = file as HttpPostedFileBase;
                if (hpImage.ContentLength == 0)
                    return Json(new { success = false });
                var extension = Path.GetExtension(hpImage.FileName).ToLower();
                var name = hpImage.FileName;
                var sr = new StorageRepository();

                var image = "";
                var thumbNail = "";
                if (Array.Exists(new[] { ".jpg", ".jpeg", ".gif", ".png" }, s => s == extension))
                {
                    var fileName = Guid.NewGuid() + "_testImage.png";
                    var fileNameSmall = fileName.Replace("_testImage.png", "_Small_testImage.png");
                    var fileNameThumbNail = fileName.Replace("_testImage.png", "_ThumbNail.png");

                    image = sr.SaveImageFromStream(hpImage.InputStream, fileName, "image/png");
                    thumbNail = sr.SaveImageFromStream(hpImage.InputStream, fileNameSmall, "image/png", 600, 600);
                    sr.SaveImageFromStream(hpImage.InputStream, fileNameThumbNail, "image/png", 200, 200);

                }
                else
                {
                    var fileName = Guid.NewGuid() + hpImage.FileName;
                    image = sr.SaveImageFromStream(hpImage.InputStream, fileName, hpImage.ContentType);
                    thumbNail = hpImage.FileName;
                }
                var jqUploadFile = new JQFileUploadFile
                {
                    url = image,
                    thumbnailUrl = thumbNail,
                    name = name,
                    type = hpImage.ContentType,
                    size = hpImage.ContentLength,
                    deleteUrl = "",
                    deleteType = "POST",
                };
                var files = new List<JQFileUploadFile> { jqUploadFile };
                return Json(new { success = true, files = files });

            }
            return Json(new { success = false });

        }

        public ActionResult Login()
        {
            return View();
        }
    }
    public class JQFileUploadFile
    {
        public string url { get; set; }
        public string thumbnailUrl { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string deleteUrl { get; set; }
        public string deleteType { get; set; }
    }

}
