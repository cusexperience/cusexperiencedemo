﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cusxp.Helpers
{
    public static class StringExtensions
    {
        public static string ToSQLString(this string sqlString)
        {
            if (!String.IsNullOrEmpty(sqlString))
                return sqlString.Replace("'", "''");
            else return null;
        }

        public static string GetExtensionFromFilename(string filename)
        {
            if (filename != null && filename.IndexOf(".", System.StringComparison.Ordinal) == -1)
                return string.Empty;

            return filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal), filename.Length - filename.LastIndexOf("."));
        }

        public static string RemovePunctutions(this string str)
        {
            var punctuations = new[] { '.', ',', '/', '\\', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '{', '}', '[', ']', ';', ':', '\'', '"', '`', '~','-','_','+','=','?' };
            var retVal = string.Join(string.Empty, str.Where(a => !punctuations.Contains(a)).ToArray());

            return retVal;
        }

        public static bool ContainsAny(this string theString, string[] keys)
        {
            foreach (var key in keys)
            {
                if (theString.Contains(key))
                {
                    return true;
                }
            }
            return false;
        }

        public static string BoldAll(this string theString, string[] keys)
        {
            var newstring = string.Empty;

            if (keys.Length == 0)
                return theString;

            foreach (var key in keys)
            {
                foreach (var word in theString.Split(' '))
                {
                    if (word.ToLower().Contains(key))
                    {
                        newstring += string.Format("<b>{0}</b>", word);
                    }
                    else
                    {
                        newstring += word;
                    }
                    newstring += " ";
                }
                //if (theString.ToLower().Contains(key))
                //{
                //    theString = theString.Replace(key.ToLower(), string.Format("<b>{0}</b>", key));
                //}
            }
            return newstring;
        }
    }
}