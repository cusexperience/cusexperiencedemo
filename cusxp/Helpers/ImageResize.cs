﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
namespace cusxp.Helpers
{
    public class ImageResize
    {
        /// <summary>
        /// Resize Image from Byte Array
        /// </summary>
        /// <param name="MaxSideSize">Maximum size of sides</param>
        /// <param name="byteArrayIn">Byte Array</param>
        /// <param name="fileName">File name</param>
        /// <returns>Byte Array representing resized image</returns>
        internal static byte[] ResizeFromByteArray(int MaxSideSize, Byte[] byteArrayIn, string fileName)
        {
            byte[] byteArray = null;  // really make this an error gif
            MemoryStream ms = new MemoryStream(byteArrayIn);
            byteArray = ImageResize.ResizeFromStream(MaxSideSize, ms, fileName);

            return byteArray;
        }

        /// <summary>
        /// Resize Image from a Stream
        /// </summary>
        /// <param name="MaxSideSize">Maximum size of sides</param>
        /// <param name="Buffer">Stream</param>
        /// <param name="fileName">File name</param>
        /// <returns>Byte Array representing resized image</returns>
        internal static byte[] ResizeFromStream(int MaxSideSize, Stream Buffer, string fileName)
        {
            byte[] byteArray = null;  // really make this an error gif

            try
            {

                Bitmap bitMap = new Bitmap(Buffer);
                int intOldWidth = bitMap.Width;
                int intOldHeight = bitMap.Height;

                int intNewWidth;
                int intNewHeight;

                int intMaxSide;

                if (intOldWidth >= intOldHeight)
                {
                    intMaxSide = intOldWidth;
                }
                else
                {
                    intMaxSide = intOldHeight;
                }

                if (intMaxSide > MaxSideSize)
                {
                    //set new width and height
                    double dblCoef = MaxSideSize / (double)intMaxSide;
                    intNewWidth = Convert.ToInt32(dblCoef * intOldWidth);
                    intNewHeight = Convert.ToInt32(dblCoef * intOldHeight);
                }
                else
                {
                    intNewWidth = intOldWidth;
                    intNewHeight = intOldHeight;
                }

                Size ThumbNailSize = new Size(intNewWidth, intNewHeight);
                System.Drawing.Image oImg = System.Drawing.Image.FromStream(Buffer);
                System.Drawing.Image oThumbNail = new Bitmap(ThumbNailSize.Width, ThumbNailSize.Height);

                Graphics oGraphic = Graphics.FromImage(oThumbNail);
                oGraphic.CompositingQuality = CompositingQuality.HighQuality;
                oGraphic.SmoothingMode = SmoothingMode.HighQuality;
                oGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Rectangle oRectangle = new Rectangle
                    (0, 0, ThumbNailSize.Width, ThumbNailSize.Height);

                oGraphic.DrawImage(oImg, oRectangle);

                MemoryStream ms = new MemoryStream();
                oThumbNail.Save(ms, ImageFormat.Jpeg);
                byteArray = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(byteArray, 0, Convert.ToInt32(ms.Length));

                oGraphic.Dispose();
                oImg.Dispose();
                ms.Close();
                ms.Dispose();
            }
            catch (Exception)
            {
                int newSize = MaxSideSize - 20;
                Bitmap bitMap = new Bitmap(newSize, newSize);
                Graphics g = Graphics.FromImage(bitMap);
                g.FillRectangle(new SolidBrush(Color.Gray), new Rectangle(0, 0, newSize, newSize));

                Font font = new Font("Courier", 8);
                SolidBrush solidBrush = new SolidBrush(Color.Red);
                g.DrawString("Failed File", font, solidBrush, 10, 5);
                g.DrawString(fileName, font, solidBrush, 10, 50);

                MemoryStream ms = new MemoryStream();
                bitMap.Save(ms, ImageFormat.Jpeg);
                byteArray = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(byteArray, 0, Convert.ToInt32(ms.Length));

                ms.Close();
                ms.Dispose();
                bitMap.Dispose();
                solidBrush.Dispose();
                g.Dispose();
                font.Dispose();

            }
            return byteArray;
        }

        /// <summary>
        /// Resize Image from Byte Array
        /// </summary>
        /// <param name="buffer">Stream</param>
        /// <param name="maxHeight">Maximum height of Image</param>
        /// <param name="maxWidth">Maximum width of Image</param>
        /// <returns>Byte Array representing resized image</returns>
        public static byte[] SaveFromStream(Stream buffer, int maxHeight, int maxWidth)
        {
            System.Drawing.Image imageToBeResized = System.Drawing.Image.FromStream(buffer);
            int imageHeight = imageToBeResized.Height;
            int imageWidth = imageToBeResized.Width;
            Bitmap bitmap;
            if (imageHeight > maxHeight || imageWidth > maxWidth)
            {
                double aspectRatio = (double)imageWidth / imageHeight;
                double boxRatio = (double)maxWidth / maxHeight;
                double scaleFactor = 0;

                if (boxRatio > aspectRatio)
                    scaleFactor = (double)maxHeight / imageHeight;
                else
                    scaleFactor = (double)maxWidth / imageWidth;

                imageWidth = (int)(imageWidth * scaleFactor);
                imageHeight = (int)(imageHeight * scaleFactor);

            }
            bitmap = new Bitmap(imageWidth, imageHeight);
            using (Graphics gr = Graphics.FromImage(bitmap))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(imageToBeResized, new Rectangle(0, 0, imageWidth, imageHeight));
                gr.Dispose();
            }


            System.IO.MemoryStream stream = new MemoryStream();
            bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            stream.Position = 0;
            byte[] image = new byte[stream.Length + 1];
            stream.Read(image, 0, image.Length);

            return image;
        }

    }
}