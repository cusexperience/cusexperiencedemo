namespace cusxp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerPackages", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AnswerPackages", "IsAdmin");
        }
    }
}
