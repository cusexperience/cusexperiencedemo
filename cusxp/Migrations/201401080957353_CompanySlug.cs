namespace cusxp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanySlug : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "Slug", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Slug");
        }
    }
}
