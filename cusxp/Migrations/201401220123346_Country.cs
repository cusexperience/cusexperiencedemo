namespace cusxp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Country : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerPackages", "Country", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AnswerPackages", "Country");
        }
    }
}
