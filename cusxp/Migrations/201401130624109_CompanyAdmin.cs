namespace cusxp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanyAdmin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "UserId", c => c.Int());
            CreateIndex("dbo.Companies", "UserId");
            AddForeignKey("dbo.Companies", "UserId", "dbo.UserProfile", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "UserId", "dbo.UserProfile");
            DropIndex("dbo.Companies", new[] { "UserId" });
            DropColumn("dbo.Companies", "UserId");
        }
    }
}
