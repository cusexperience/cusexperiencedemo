namespace cusxp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerPackages",
                c => new
                    {
                        AnswerPackageId = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        Image = c.String(),
                        ImageDescription = c.String(),
                        GivenName = c.String(),
                        SurName = c.String(),
                        Email = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AnswerPackageId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        AnswerId = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        Comments = c.String(),
                        Score = c.String(),
                        AnswerPackage_AnswerPackageId = c.Int(),
                    })
                .PrimaryKey(t => t.AnswerId)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.AnswerPackages", t => t.AnswerPackage_AnswerPackageId)
                .Index(t => t.QuestionId)
                .Index(t => t.AnswerPackage_AnswerPackageId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        QuestionId = c.Int(nullable: false, identity: true),
                        Position = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Type = c.String(),
                        Options = c.String(),
                        FormId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionId)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .Index(t => t.FormId);
            
            CreateTable(
                "dbo.Forms",
                c => new
                    {
                        FormId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.FormId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Image = c.String(),
                        FlipImage = c.String(),
                        Address = c.String(),
                        GoogleMapsAddress = c.String(),
                        PhoneNumber = c.String(),
                        EmailAddress = c.String(),
                        FormId = c.Int(nullable: false),
                        Company_CompanyId = c.Int(),
                    })
                .PrimaryKey(t => t.LocationId)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .ForeignKey("dbo.Companies", t => t.Company_CompanyId)
                .Index(t => t.FormId)
                .Index(t => t.Company_CompanyId);
            
            CreateTable(
                "dbo.Appreciations",
                c => new
                    {
                        AppreciationId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Display = c.String(),
                        ExpirationDate = c.DateTime(nullable: false),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.AppreciationId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        AppreciationId = c.Int(nullable: false),
                        RegisteredName = c.String(),
                        BrandName = c.String(),
                        Country = c.String(),
                        LogoImage = c.String(),
                    })
                .PrimaryKey(t => t.CompanyId)
                .ForeignKey("dbo.Appreciations", t => t.AppreciationId, cascadeDelete: true)
                .Index(t => t.AppreciationId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        EmailAddress = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false, maxLength: 20),
                        Blocked = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Locations", "Company_CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Companies", "AppreciationId", "dbo.Appreciations");
            DropForeignKey("dbo.AnswerPackages", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "FormId", "dbo.Forms");
            DropForeignKey("dbo.Answers", "AnswerPackage_AnswerPackageId", "dbo.AnswerPackages");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "FormId", "dbo.Forms");
            DropIndex("dbo.Locations", new[] { "Company_CompanyId" });
            DropIndex("dbo.Companies", new[] { "AppreciationId" });
            DropIndex("dbo.AnswerPackages", new[] { "LocationId" });
            DropIndex("dbo.Locations", new[] { "FormId" });
            DropIndex("dbo.Answers", new[] { "AnswerPackage_AnswerPackageId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropIndex("dbo.Questions", new[] { "FormId" });
            DropTable("dbo.UserProfile");
            DropTable("dbo.Companies");
            DropTable("dbo.Appreciations");
            DropTable("dbo.Locations");
            DropTable("dbo.Forms");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
            DropTable("dbo.AnswerPackages");
        }
    }
}
