using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Web.Security;
using WebMatrix.WebData;
using cusxp.DAL;
using cusxp.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace cusxp.Migrations
{


    internal sealed class Configuration : DbMigrationsConfiguration<cusxp.DAL.CusXpContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(cusxp.DAL.CusXpContext context)
        {
            //SeedMembership(context);
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }

        private const string administratorRole = "Administrator";
        private const string userRole = "User";
        private const string companyAdminRole = "CompanyAdmin";

        private void SeedMembership(CusXpContext context)
        {
            WebSecurity.InitializeDatabaseConnection("CusXpContext", "UserProfile",
                                                     "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists(administratorRole))
                Roles.CreateRole(administratorRole);
            if (!Roles.RoleExists(userRole))
                Roles.CreateRole(userRole);
            if (!Roles.RoleExists(companyAdminRole))
                Roles.CreateRole(companyAdminRole);

            if (!WebSecurity.UserExists("administrator"))
            {
                WebSecurity.CreateUserAndAccount("administrator", "test123",
                                                 new
                                                 {
                                                     FirstName = "Prabhu",
                                                     LastName = "Sengal",
                                                     EmailAddress = "prabhu.sengal@gmail.com",
                                                     PhoneNumber = "+6597270011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }

            if (!Roles.GetRolesForUser("administrator").Contains(administratorRole))
                Roles.AddUsersToRole(new[] { "administrator" }, administratorRole);

            // 1-Nov-2013
            if (!WebSecurity.UserExists("rajna123"))
            {
                WebSecurity.CreateUserAndAccount("rajna123", "password",
                                                 new
                                                 {
                                                     FirstName = "Rajna",
                                                     LastName = "Ranjith",
                                                     EmailAddress = "rajna.ranjith@gmail.com",
                                                     PhoneNumber = "+659450011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }
            if (!Roles.GetRolesForUser("rajna123").Contains(userRole))
                Roles.AddUsersToRole(new[] { "rajna123" }, userRole);

            if (!WebSecurity.UserExists("geadmin"))
            {
                WebSecurity.CreateUserAndAccount("geadmin", "password",
                                                 new
                                                 {
                                                     FirstName = "Great Eastern",
                                                     LastName = "Admin",
                                                     EmailAddress = "ge@cusjo.com",
                                                     PhoneNumber = "+659450011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }
            if (!Roles.GetRolesForUser("geadmin").Contains(companyAdminRole))
                Roles.AddUsersToRole(new[] { "geadmin" }, companyAdminRole);

            if (!WebSecurity.UserExists("msadmin"))
            {
                WebSecurity.CreateUserAndAccount("msadmin", "password",
                                                 new
                                                 {
                                                     FirstName = "Manoj Sharma",
                                                     LastName = "Admin",
                                                     EmailAddress = "manoj@cusjo.com",
                                                     PhoneNumber = "+659450011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }
            if (!Roles.GetRolesForUser("msadmin").Contains(companyAdminRole))
                Roles.AddUsersToRole(new[] { "msadmin" }, companyAdminRole);

            if (!WebSecurity.UserExists("tspadmin"))
            {
                WebSecurity.CreateUserAndAccount("tspadmin", "password",
                                                 new
                                                 {
                                                     FirstName = "TSP",
                                                     LastName = "Admin",
                                                     EmailAddress = "tsp@cusjo.com",
                                                     PhoneNumber = "+659450011",
                                                     Blocked = false,
                                                     DateCreated = DateTime.Now
                                                 });
            }
            if (!Roles.GetRolesForUser("tspadmin").Contains(companyAdminRole))
                Roles.AddUsersToRole(new[] { "tspadmin" }, companyAdminRole);

            var userID = WebSecurity.GetUserId("administrator");

            var prabhusengal = context.UserProfiles.SingleOrDefault(a => a.UserId == userID);

            var geAdminID = WebSecurity.GetUserId("geadmin");

            var geAdmin = context.UserProfiles.SingleOrDefault(a => a.UserId == geAdminID);

            var msAdminID = WebSecurity.GetUserId("msadmin");

            var msAdmin = context.UserProfiles.SingleOrDefault(a => a.UserId == msAdminID);

            var tspAdminID = WebSecurity.GetUserId("tspadmin");

            var tspAdmin = context.UserProfiles.SingleOrDefault(a => a.UserId == tspAdminID);

            var rnd = new Random(Environment.TickCount);

            var form = new Form
            {
                Questions = new List<Question>
                        {
                            new Question
                                {
                                    Description = "How was your overall experience with us?",
                                    Type = QuestionTypes.Scale.ToString(),
                                    Position = 1,
                                    Options = null,
                                },
                            new Question
                                {
                                    Description = "Which staff(s) did you interact with?",
                                    Type = QuestionTypes.Comment.ToString(),
                                    Position = 2,
                                    Options = null
                                },
                            new Question
                                {
                                    Description = "What did you like the most?",
                                    Type = QuestionTypes.Select.ToString(),
                                    Position = 3,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                            new Question
                                {
                                    Description = "What did you dislike the most?",
                                    Type = QuestionTypes.Select.ToString(),
                                    Position = 4,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                            new Question
                                {
                                    Description = "What can we do to further improve your experience?",
                                    Type = QuestionTypes.Comment.ToString(),
                                    Position = 5,
                                    Options = null
                                },
                            new Question
                                {
                                    Description = "What do you feel you value the most?",
                                    Type = QuestionTypes.MultiSelect.ToString(),
                                    Position = 6,
                                    Options = "People,Product,Process,Venue,Others"
                                },
                        }
            };

            var appreciation = new cusxp.Models.Appreciation
            {
                Title = "Complimentary $10 Voucher*",
                Display = "",
                ExpirationDate = new DateTime(2015, 12, 31),
                Image = @"images/AppreciationImage.png",
            };

            var company = new cusxp.Models.Company
            {
                RegisteredName = "Manoj Sharma",
                BrandName = "Manoj Sharma",
                Country = "Singapore",
                LogoImage = @"images/CompanyLogoImage.png",
                Appreciation = appreciation,
                Slug = "manojsharma",
                UserProfile = geAdmin
            };

            company.Locations = new List<Location>
                {
                    new Location
                        {
                            Name = "General Feedback",
                            Image = @"images/LocationImage.jpg",
                            FlipImage = @"images/LocationFlipImage.png",
                            Address = @"75 Bukit Timah Road, #05-10 Boon Siew Building, Singapore 229833",
                            GoogleMapsAddress = @"http://goo.gl/maps/3BZeY",
                            PhoneNumber = "+6563388979",
                            EmailAddress = "kelvin@manojsharma.com",
                            Form =  form
                        }
                };

            var existingCompany = context.Companies.SingleOrDefault(a => a.Slug == company.Slug);
            if (existingCompany == null)
                context.Companies.Add(company);

            var appreciation2 = new cusxp.Models.Appreciation
            {
                Title = "Complimentary $10 Voucher*",
                Display = "",
                ExpirationDate = new DateTime(2015, 12, 31),
                Image = @"images/GE-AppreciationImage.png",
            };

            var company2 = new cusxp.Models.Company
            {
                RegisteredName = "Great Eastern",
                BrandName = "Great Eastern",
                Country = "Singapore",
                LogoImage = @"images/GE-CompanyLogo.png",
                Appreciation = appreciation2,
                Slug = "greateastern",
                UserProfile = msAdmin
            };

            company2.Locations = new List<Location>
                {
                    new Location
                        {
                            Name = "General Experience",
                            Image = @"images/GE-LocationImage-GeneralExperience.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Customer Service Center",
                            Image = @"images/GE-LocationImage-CusotomerService.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Call Center",
                            Image = @"images/GE-LocationImage-CallCenter.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Email Center",
                            Image = @"images/GE-LocationImage-EmailCentre.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        }
                };
            existingCompany = context.Companies.SingleOrDefault(a => a.Slug == company2.Slug);
            if (existingCompany == null)
                context.Companies.Add(company2);

            var appreciation3 = new cusxp.Models.Appreciation
            {
                Title = "Complimentary $10 Voucher*",
                Display = "",
                ExpirationDate = new DateTime(2015, 12, 31),
                Image = @"images/GE-AppreciationImage.png",
            };

            var company3 = new cusxp.Models.Company
            {
                RegisteredName = "TSP",
                BrandName = "TSP",
                Country = "Singapore",
                LogoImage = @"images/GE-CompanyLogo.png",
                Appreciation = appreciation3,
                Slug = "tsp",
                UserProfile = tspAdmin
            };

            company3.Locations = new List<Location>
                {
                    new Location
                        {
                            Name = "General Experience",
                            Image = @"images/GE-LocationImage-GeneralExperience.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Customer Service Center",
                            Image = @"images/GE-LocationImage-CusotomerService.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Call Center",
                            Image = @"images/GE-LocationImage-CallCenter.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        },
                    new Location
                        {
                            Name = "Email Center",
                            Image = @"images/GE-LocationImage-EmailCentre.png",
                            FlipImage = @"images/GE-LocationFlipImage.png",
                            Address = @"Great Eastern Life Assurance Co Ltd, Singapore 048569",
                            GoogleMapsAddress = @"http://goo.gl/maps/axK1D",
                            PhoneNumber = "+6562482888",
                            EmailAddress = "wecare-sg@greateasternlife.com",
                            Form = form
                        }
                };
            existingCompany = context.Companies.SingleOrDefault(a => a.Slug == company3.Slug);
            if (existingCompany == null)
                context.Companies.Add(company3);

            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
            }
        }
    }
}
