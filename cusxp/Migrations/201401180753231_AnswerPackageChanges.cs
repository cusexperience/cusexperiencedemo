namespace cusxp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnswerPackageChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerPackages", "Image2", c => c.String());
            AddColumn("dbo.AnswerPackages", "ImageDescription2", c => c.String());
            AddColumn("dbo.AnswerPackages", "Image3", c => c.String());
            AddColumn("dbo.AnswerPackages", "ImageDescription3", c => c.String());
            AddColumn("dbo.AnswerPackages", "ContactNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AnswerPackages", "ContactNumber");
            DropColumn("dbo.AnswerPackages", "ImageDescription3");
            DropColumn("dbo.AnswerPackages", "Image3");
            DropColumn("dbo.AnswerPackages", "ImageDescription2");
            DropColumn("dbo.AnswerPackages", "Image2");
        }
    }
}
