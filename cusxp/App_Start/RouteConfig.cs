﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace cusxp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
                name: "ManojSharma",
                url: "manojsharma",
                defaults: new { controller = "Home", action = "Index", survey = "manojsharma" }
            );

            routes.MapRoute(
                name: "GreatEastern",
                url: "greateastern",
                defaults: new { controller = "Home", action = "Index", survey = "greateastern" }
            );

            routes.MapRoute(
                name: "tsp",
                url: "tsp",
                defaults: new { controller = "Home", action = "Index", survey = "tsp" }
            );

            routes.MapRoute(
                name: "ViewSurvey",
                url: "{survey}",
                defaults: new { controller = "Home", action = "Index", survey = "" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}